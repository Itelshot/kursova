<?php

namespace Tests\Feature;

use App\Models\ticket;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IntegrationTest extends TestCase
{
    private $ticket;

    public function setUp(): void
    {
        parent::setUp();
        $this->ticket = new ticket();
    }

    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testTest()
    {
        $result = $this->ticket->show('Vasul Symonenko')->toArray()[0];
        $this->assertEquals('Vasul Symonenko', $result->name);
    }
}
