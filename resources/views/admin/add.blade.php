@extends('admin.schema.schema')

@section('title-block','Admin add')

@section('content')
    <div>
        <h1>Введіть інформацію про відвідувача</h1>
        <form action="{{route('attraction.store')}}" method="post">
            @csrf
            <p>
                <label for="name">Ім'я відвідувача</label>
                <input type="text" placeholder="Ім'я відвідувача" id="name" name="name" required>
            </p>
            <p>
                <label for="attraction">Атракціон</label>
                <select id="attraction" name="attraction" required>
                    @foreach($attractions as $attraction)
                        <option value='{{ $attraction->attraction_id }}'>{{ $attraction->attraction }}</option>
                    @endforeach
                </select>
            </p>

            <p>
                <label for="cost">Цiна</label>
                <input type="text" name="cost" id="cost" placeholder="Цiна" required>
            </p>
            <p>
                <label for="date">Дата</label>
                <input type="date" placeholder="Дата" id="date" name="date" required>
            </p>
            <p>
                <button type="submit">Додати</button>
                <a href="{{route('attraction.index')}}">Назад</a>
            </p>
        </form>
    </div>
@endsection
