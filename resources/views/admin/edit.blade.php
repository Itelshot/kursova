@extends('admin.schema.schema')
@foreach($result as $res)
    @section('title-block','Edit '.$res->name)

@section('content')
    <div>
        <h1>Введіть інформацію про відвідувача</h1>
        <form action="{{route('attraction.update', ['attraction' => $res->ticket_id] )}}" method="post">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <input type="hidden" value="{{$res->ticket_id}}" name="id">
            <p>
                <label for="name">Ім'я відвідувача</label>
                <input type="text" placeholder="Ім'я відвідувача" id="name" name="name" value="{{$res->name}}" required>
            </p>
            <p>
                <label for="attraction">Атракціон</label>
                <select id="attraction" name="attraction" required>
                    @foreach($attractions as $attraction)
                        <option value='{{ $attraction->attraction_id }}'>{{ $attraction->attraction }}</option>
                    @endforeach
                </select>
            </p>

            <p>
                <label for="cost">Цiна</label>
                <input type="text" name="cost" id="cost" placeholder="Цiна" value="{{$res->cost}}" required>
            </p>
            <p>
                <label for="date">Дата</label>
                <input type="date" placeholder="Дата" id="date" name="date" value="{{$res->date}}" required>
            </p>
            <p>
                <button type="submit" name="edit">Редагувати</button>
                <a href="{{route('attraction.index')}}">Назад</a>
            </p>
        </form>
    </div>
@endsection
@endforeach
