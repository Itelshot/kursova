@extends('admin.schema.schema')

@section('title-block','Admin')

@section('content')
    <p>
        <a href="{{route('attraction.index')}}">Головна</a>
        |
        <a href="{{route('attraction.create')}}">Додати</a>
        |
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    </p>
@endsection
