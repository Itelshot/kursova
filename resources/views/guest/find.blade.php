@extends('guest.schema.schema')

@section('title-block','Find')

@section('content')
    <form action="{{route('find-form')}}" method="post">
        @csrf
        <p>
        <label for="id">Ім'я відвідувача:</label>
            <input type="text" name="name">
            <button type="submit">Пошук</button>
        </p>
    </form>
    <p>
        <a href="{{route("index")}}">Назад</a>
    </p>
@endsection
