<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ticket extends Model
{
    use HasFactory;

    protected $table = 'ticket';
    public $timestamps = false;

    public function index()
    {
       return DB::table('ticket as t')
        ->join('attraction as a', 'a.attraction_id', '=', 't.attraction_type')
        ->select('t.*', 'a.*')
           ->get();
    }

    public function show($name)
    {
        return  DB::table('ticket as t')
            ->join('attraction as a', 'a.attraction_id', '=', 't.attraction_type')
            ->select('t.*', 'a.*')
            ->where('t.name','like', $name.'%')
            ->get();
    }

    public function showById($id)
    {
        return DB::table('ticket as t')
            ->join('attraction as a', 'a.attraction_id', '=', 't.attraction_type')
            ->select('t.*', 'a.*')
            ->where('t.ticket_id', $id)
            ->get();
    }
}
