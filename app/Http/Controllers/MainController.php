<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    protected function show()
    {
        return view('guest.index', ['data' => (new \App\Models\ticket)->index()]);
    }


    public function find(Request $req)
    {
        $name = $req->input('name');
        return view('guest.find', ['data' => (new \App\Models\ticket)->show($name)]);
    }
}
