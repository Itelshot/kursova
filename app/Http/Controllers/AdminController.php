<?php

namespace App\Http\Controllers;

use App\Models\Attraction;
use App\Models\ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index', ['data' => (new \App\Models\ticket)->index()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add', ['attractions' => (new \App\Models\attraction)->attraction()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new ticket();
        $add->name = $request->input('name');
        $add->attraction_type = $request->input('attraction');
        $add->cost = $request->input('cost');
        $add->date = $request->input('date');
        $add->save();
        return redirect()->route('attraction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', [
            'result' => (new \App\Models\ticket())->showById($id),
            'attractions' => (new \App\Models\attraction)->attraction()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('ticket')
            ->where('ticket.ticket_id', $id)
            ->update([
                'attraction_type' => $request->input('attraction'),
                'cost' => $request->input('cost'),
                'date' => $request->input('date'),
                'name' => $request->input('name'),
            ]);
        return redirect()->route('attraction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('ticket')->where('ticket_id', '=', $id)->delete();
        return redirect()->route('attraction.index');
    }
}
